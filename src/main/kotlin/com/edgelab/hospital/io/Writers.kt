package com.edgelab.hospital.io

import com.edgelab.hospital.care.HealthStatus

fun List<HealthStatus>.format(): String = HealthStatus.values()
    .associate { it.acronym to 0 }
    .plus(this.groupingBy { it.acronym }.eachCount())
    .map { "${it.key}:${it.value}" }
    .joinToString(separator = ",")
