package com.edgelab.hospital

import com.edgelab.hospital.care.Hospital
import com.edgelab.hospital.care.MiracleMaker
import com.edgelab.hospital.io.asDrugSet
import com.edgelab.hospital.io.asPatientList
import com.edgelab.hospital.io.format

fun main(args: Array<String>) {
    println(
        if (args.isEmpty())
            throw IllegalArgumentException("Please provide at least a patient list as argument")
        else
            Hospital
                .create(miracleMaker = MiracleMaker.oneEvery(1000000))
                .treat(
                    patients = args[0].asPatientList(),
                    drugs = if (args.size > 1) args[1].asDrugSet() else emptySet()
                ).format()
    )
}
