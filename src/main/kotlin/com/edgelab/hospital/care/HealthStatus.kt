package com.edgelab.hospital.care

enum class HealthStatus(val acronym: String) {
    Fever("F"),
    Healthy("H"),
    Diabetes("D"),
    Tuberculosis("T"),
    Dead("X");
}
